#include <stdio.h>
#include <ctype.h>

int main(void)
{
int wordcount = 0; // 총 단어 개수
int ulwdcount = 0; // 대문자로 시작해 소문자로 끝나는 단어 개수

// 문제풀이 시작
int q; // 입력 문자
int a; // 시작 문자
int z; // 끝 문자
int space = 1; // 직전 문자가 공백
while((q = getchar()) != EOF) {
if (isspace(q)) { // 공백 입력
if (!space) { // 이전에 공백이 아니였으면
if ('A' <= a && a <= 'Z' && 'a' <= z && z <= 'z') {
ulwdcount++;
a = z = 0;
}
space = 1;
}
} else {
if (space) {
wordcount++;
a = q;
space = 0;
} else {
z = q;
}
}
}

printf("%d\n", wordcount);
printf("%d\n", ulwdcount);
return 0;
}
